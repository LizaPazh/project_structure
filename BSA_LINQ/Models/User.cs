﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSA_LINQ.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public string RegisteredAt { get; set; }
        public int? TeamId { get; set; }
        public IEnumerable<ProjectTask> ProjectTasks { get; set; }
    }
}
