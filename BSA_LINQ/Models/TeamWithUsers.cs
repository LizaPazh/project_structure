﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSA_LINQ.Models
{
    public class TeamWithUsers
    {
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public IEnumerable<User> Users { get; set; }
    }
}
