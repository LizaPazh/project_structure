﻿using BSA_LINQ.Models;

namespace BSA_LINQ
{
    public class UserWithParameters
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int LastProjectTasks { get; set; }
        public int NotCompletedOrCanceledTasks { get; set; }
        public ProjectTask LongestTask { get; set; }
    }
}