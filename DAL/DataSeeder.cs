﻿using DAL.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DAL
{
    public class DataSeeder
    {
        public IEnumerable<Project> Projects { get; private set; }
        public IEnumerable<ProjectTask> Tasks { get; private set; }
        public IEnumerable<User> Users { get; private set; }
        public IEnumerable<Team> Teams { get; private set; }

        public DataSeeder()
        { 
            var projects = LoadProjects();
            var users = LoadUsers();
            var tasks = LoadTasks();
            var teams = LoadTeams();

            tasks = tasks.Join(users, t => t.PerformerId, u => u.Id, (t, p) => { t.Performer = p; return t; });
            Tasks = tasks;
            users = users.GroupJoin(tasks, u => u.Id, t => t.PerformerId, (u, t) => { u.ProjectTasks = t; return u; });
            Users = users;
            teams = teams.GroupJoin(users, t => t.Id, u => u.TeamId, (t, p) => { t.Users = p; return t; });
            Teams = teams;
            projects = projects.GroupJoin(tasks, p => p.Id, t => t.ProjectId, (p, taskList) => { p.ProjectTasks = taskList; return p; });
            projects = projects.Join(users, p => p.AuthorId, u => u.Id, (p, u) => { p.Author = u; return p; });
            projects = projects.Join(teams, p => p.TeamId, t => t.Id, (p, t) => { p.Team = t; return p; });
            Projects = projects;
        }
        private IEnumerable<Project> LoadProjects()
        {     
            var fileName = "Data\\projects.json";
            using (StreamReader file = new StreamReader(fileName))
            {
                return JsonConvert.DeserializeObject<IEnumerable<Project>>(file.ReadToEnd());
            }
        }

        private IEnumerable<ProjectTask> LoadTasks()
        {
            var fileName = "Data\\tasks.json";
            using (StreamReader file = new StreamReader(fileName))
            {
                return JsonConvert.DeserializeObject<IEnumerable<ProjectTask>>(file.ReadToEnd());
            }
        }

        private IEnumerable<Team> LoadTeams()
        {
            var fileName = "Data\\teams.json";
            using (StreamReader file = new StreamReader(fileName))
            {
                return JsonConvert.DeserializeObject<IEnumerable<Team>>(file.ReadToEnd());
            }
        }

        private IEnumerable<User> LoadUsers()
        {
            var fileName = "Data\\users.json";
            using (StreamReader file = new StreamReader(fileName))
            {
                return JsonConvert.DeserializeObject<IEnumerable<User>>(file.ReadToEnd());
            }
        }
    }
}
