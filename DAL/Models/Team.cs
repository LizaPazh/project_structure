﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public class Team : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public IEnumerable<User> Users { get; set; }
    }
}
