﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DAL
{
    public class RepositoryBase<T> : IRepository<T> where T : class, IEntity
    {
        private readonly List<T> _data;
        public RepositoryBase(List<T> data)
        {
            _data = data;
        }
        public void Create(T entity)
        {
            _data.Add(entity);
        }

        public void Delete(int id)
        {
            var entity = _data.FirstOrDefault(x => x.Id == id);

            if (entity != null)
            {
                _data.Remove(entity);
            }
        }

        public IQueryable<T> GetAll(Expression<Func<T, bool>> filter = null)
        {
            IQueryable<T> query = _data.AsQueryable();

            if (filter != null)
            {
                query = query.Where(filter);
            }
            return query;
        }

        public T GetById(int id)
        {
            return _data.SingleOrDefault(t => t.Id == id);
        }

        public void Update(T entity)
        {
            var indexToUpdate = _data.FindIndex(t => t.Id == entity.Id);

            _data[indexToUpdate] = entity;
        }
    }
}
