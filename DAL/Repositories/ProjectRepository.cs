﻿using DAL.Models;
using System.Collections.Generic;

namespace DAL.Repositories
{
    public interface IProjectRepository : IRepository<Project>
    {

    }
    public class ProjectRepository : RepositoryBase<Project>, IProjectRepository
    {
        public ProjectRepository(List<Project> data)
              : base(data)
        { }
    }
}
