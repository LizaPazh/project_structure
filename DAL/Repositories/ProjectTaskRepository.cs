﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public interface IProjectTaskRepository : IRepository<ProjectTask>
    {

    }
    public class ProjectTaskRepository : RepositoryBase<ProjectTask>, IProjectTaskRepository
    {
        public ProjectTaskRepository(List<ProjectTask> data)
              : base(data)
        { }
    }
}
