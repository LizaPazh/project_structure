﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public interface ITeamRepository : IRepository<Team>
    {

    }
    public class TeamRepository : RepositoryBase<Team>, ITeamRepository
    {
        public TeamRepository(List<Team> data)
              : base(data)
        { }
    }
}
