﻿using BLL.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Interfaces
{
    public interface IUserService
    {
        UserDTO GetUserById(int userId);
        IEnumerable<UserDTO> GetAllUsers();
        void CreateUser(UserDTO user);
        void UpdateUser(UserDTO user);
        void DeleteUser(int userId);
        IEnumerable<UserDTO> UsersByABCWithTasksSortedByName();
        UserWithParameters GetUsersCharacteristics(int userId);
    }
}
