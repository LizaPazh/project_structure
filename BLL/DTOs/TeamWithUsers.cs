﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTOs
{
    public class TeamWithUsers
    {
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public IEnumerable<UserDTO> Users { get; set; }
    }
}
